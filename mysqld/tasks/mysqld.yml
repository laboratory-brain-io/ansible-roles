---
- name: install mysql
  yum:
    name: "{{ item }}"
    state: present
  with_items:
    - "{{ mysql_common_packages }}"
  register: installation_mysqld
  ignore_errors: true

- name: change mysql data directory in init script
  sudo: yes
  remote_user: ec2-user
  lineinfile:
    dest: /etc/rc.d/init.d/mysqld
    state: present
    backup: yes
    backrefs: yes
    regexp: '^get_mysql_option mysqld datadir\s*(.+)'
    line: "{{ 'get_mysql_option mysqld datadir \"' + mysql_datadir + '\"' }}"
  notify: restart mysqld
  ignore_errors: yes

- name: install common-packages for mysql from pip
  pip:
    name: "{{ item }}"
    state: present
  with_items:
    - "{{ mysql_pip_packages }}"
  ignore_errors: true

- name: set my.cnf from template
  template:
    src: my.cnf.j2
    dest: /etc/my.cnf
    backup: yes
  notify:
    - restart mysqld
  ignore_errors: yes

- name: start mysqld
  service:
    name: mysqld
    state: started
    enabled: yes
  when: installation_mysqld.changed == true
  ignore_errors: yes

- name: mysql root password to confirm whether modified or not
  shell: mysql -u root -e "quit"
  register: modified_root_password
  ignore_errors: yes

- name: update mysql root password
  mysql_user:
    name: root
    password: "{{ mysql_root_password }}"
    host: localhost
    state: present
  when: mysql_root_password is defined and mysql_root_password is defined and modified_root_password | success
  ignore_errors: yes

- name: delete anonymous user
  mysql_user:
    name: ''
    login_user: root
    login_password: "{{ mysql_root_password }}"
    state: absent
  ignore_errors: yes

- name: add mysql users
  mysql_user:
    name: "{{ item.name }}"
    password: "{{ item.password }}"
    priv: "{{ item.privileges | join('/') }}"
    login_user: root
    login_password: "{{ mysql_root_password }}"
    state: present
  with_items:
    - "{{ mysql_users }}"
  when: mysql_users is defined and mysql_root_password is defined
  ignore_errors: yes

- name: create databases
  mysql_db:
    name: "{{ item.name }}"
    state: present
    login_host: localhost
    login_port: "{{ mysql_port }}"
    login_user: root
    login_password: "{{ mysql_root_password }}"
  with_items:
    - "{{ mysql_databases }}"
  when: mysql_databases is defined and mysql_root_password is defined
  ignore_errors: yes
